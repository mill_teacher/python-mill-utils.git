from setuptools import setup, find_packages

setup(
    name="mill-utils",
    version='1.1',
    description="Machine learning Toolkit",
    url="https://gitee.com/mill_teacher/python-mill-utils.git",
    author="mill",
    author_email="563084506@qq.com",
    license="MIT",
    packages=find_packages()
)