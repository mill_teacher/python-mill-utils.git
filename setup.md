### 安装上传工具

```shell
pip install twine
```

### 打包命令

```shell
python setup.py sdist bdist_wheel
```

### 上传

```shell
twine upload dist/*
```

### 免输密码
> ~/.pypirc
```shell
[distutils]
index-servers =
    pypi
    pypitest

[pypi]
username: 
password: 

[pypitest]
repository: https://test.pypi.org/legacy/
username: 
password: 
```